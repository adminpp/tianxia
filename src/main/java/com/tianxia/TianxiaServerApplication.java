package com.tianxia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class TianxiaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TianxiaServerApplication.class, args);
    }

}
